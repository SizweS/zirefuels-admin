import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'zirefuels-admin',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
